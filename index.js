/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"

];


let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
// Solution
        

        function register(userName){

            let isUserExist = registeredUsers.includes(userName);

            if (isUserExist) {

                alert("Registration failed. Username already exists!");

            } else {

                registeredUsers.push(userName);
                alert("Thank you for registering!");
                console.log(registeredUsers);

            }
        } // register(); - comment out this 1st for because it will be invoking in console
        // also if you invoke it here the last[index] would be an undefined


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.
*/
// Solution
        function addFriend(userName){

            let isUserExist = registeredUsers.includes(userName);

            if (isUserExist) { 
                friendsList.push(userName);
                alert(`You Have added ${userName} as a friend!`);
                console.log(friendsList);


            } else {
                alert(`User ${userName} not found.`)
            }

        } // addFriend(); - comment out this 1st for because it will be invoking in console
        // also if you invoke it here the last[index] would be an undefined


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    // Solution
        function showFriendsList(){

            friendsList.forEach(function(showMyFriends){
                console.log(showMyFriends);
            })

            if(friendsList.length === 0){
                alert("You currently have 0 friends. Add one first.");
            }

        }showFriendsList();

    // Other Solution :
    /*
        Solution by Instructor Mam Miah

                function displayFriends(){

                        if(friendsList.length > 0){

                            friendsList.forEach(function(friend){

                                console.log(friend);

                            })


                        } else {
                            alert(`You have ${friendsList.length} friends. Add one first.`);
                        }

                    };
    */





/* 
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/
    
    // Solution
        function showHowManyFriends() {

            if(friendsList.length === 0){
                alert("You currently have 0 friends. Add one first.");
            }
            if(friendsList.length > 0){
                alert("You currently have " + friendsList.length + " friends.");
            }

        }showHowManyFriends();
    // Other Solution :
    /*
        Solution by Instructor Mam Miah 

                function displayNumberOfFriends(){

                       if(friendsList.length > 0){

                           alert(`You currently have ${friendsList.length} friends.`);


                       } else {
                           alert(`You have ${friendsList.length} friends. Add one first.`);
                       }

                   };
    */


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.
*/

    // Solution
            function delLastAddedFriend() {

                friendsList.pop();
                if(friendsList.length === 0) {
                     alert("You currently have 0 friends. Add one first.");
                }
                console.log(friendsList);

            } delLastAddedFriend();

    // Other Solution :
    /*
        Solution by Instructor Mam Miah

                function deleteFriend(){

                       if(friendsList.length > 0){

                           friendsList.pop();

                       } else {
                           alert(`You have ${friendsList.length} friends. Add one first.`)
                       }

                   };
    */